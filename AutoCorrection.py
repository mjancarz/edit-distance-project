#!/usr/bin/python

from Levenshtein import getClosestToGivenWord
import time
from sys import argv


if "-h" in argv or "--help" in argv:
	print "\nAplikacja prezentujaca jedno z zastosowan odleglosci edycyjnej. \
Podaj dowolne slowo z jezyka angielskiego (najlepiej z literowka), \
a program postara sie znalezc jemu najblizsze z dostepnej bazy \
slow. Domyslne uruchamianie bez argumentow. \
\n\nDostepne argumenty:\n\
-h/--help - pomoc"
	exit()


try:
	f = open("engWords.txt", 'r')
except:
	print "Wystapil blad podczas otwierania pliku"
	exit()

listOfWords = []

for word in f:
	listOfWords.append(word.strip())


print "Slow w bazie:", len(listOfWords)
print '\n"/quit" zamyka program'

while True:
	print "________________________"
	string = raw_input("\nWprowadz jakies angielskie slowko: ")

	if string == "/quit":
		break

	start = time.time()

	word = getClosestToGivenWord(string.strip(), listOfWords)

	end = time.time()

	print '\nCzy chodzilo Ci o "%s"?' % (word.strip())

	print "Czas dopasowania: %.3f" % (end - start)

