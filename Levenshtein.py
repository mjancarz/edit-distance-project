#!/usr/bin/python

import difflib
#potrzebna do znalezienia najblizszego alfabetycznie ciagu znaku
import re
#biblioteka do obslugi wyrazen regularnych
import unittest
#biblioteka testow jednostkowych
import time
#biblioteka uzyta, aby zmiezyc czas przeprowadzania testow
from sys import argv
#dostep do listy argumentow podanych w linii polecen
import getopt, sys
#biblioteka do obslugi argumentow i opcji podawanych w linii polecen


def getEditDistance(string1, string2, returnOperations=False):
	"""
	Funkcja zwraca stopien roznosci dwoch podanych ciagow znakowych 
	w postaci liczby calkowitej - wersja iteracyjna

	W przypadku podania jako trzeci parametr wartosci True
	Zwraca liste operacji przejscia z napisu 1 do napisu 2 
	zlozona z trzyelemenotych pol:

	0 - string okreslajacy operacje
	1 - string wejsciowy operacji
	2 - string wyjsciowy operacji
	"""

	if not isinstance(string1, str):
		string1 = str(string1)

	if not isinstance(string2, str):
		string2 = str(string2)

	t = [[0 for x in xrange(len(string2)+1)] 
			for x in xrange(len(string1)+1)]

	for i in range(0, len(string1)+1):
		t[i][0] = i

	for i in range(0, len(string2)+1):
		t[0][i] = i

	for i in range(1, len(string1)+1):
		for j in range(1, len(string2)+1):
			if string1[i-1] == string2[j-1]:
				cost = 0
			else:
				cost = 1

			t[i][j] = min(t[i-1][j] + 1,
							  t[i][j-1] + 1,
							  t[i-1][j-1] + cost)

	if not returnOperations:

		return t[-1][-1]

	else:

		ops = []

		i = len(string1)
		j = len(string2)

		ops.append([i, j])

		while i+j != 0:

			if t[i-1][j-1] == min(t[i-1][j-1], 
									  t[i][j-1], t[i-1][j]):

				ops.append([i-1, j-1])

				i-=1
				j-=1

			elif t[i][j-1] == min(t[i-1][j-1], 
									  t[i][j-1], t[i-1][j]):

				ops.append([i, j-1])

				j-=1

			elif t[i-1][j] == min(t[i-1][j-1], t[i][j-1], 
									  t[i-1][j]):

				ops.append([i-1, j])

				i-=1

		ops = ops[::-1]

		offset = 0

		opStrings = []

		for i, v in enumerate(ops):
			if i < len(ops)-1:
				if v[0] != ops[i+1][0] and v[1] != ops[i+1][1]:
					if not t[ops[i+1][0]][ops[i+1][1]] == t[v[0]][v[1]]:

						pre_string1 = string1

						char1 = string1[v[0]+offset]
						char2 = string2[v[1]]

						listFromString = list(string1)

						listFromString[v[0]+offset] = char2

						string1 = ''.join(listFromString)

						opStrings.append([
							("Zamiana '"+char1+"' na '" +char2+"'").ljust(18),
							pre_string1, string1])
				
				elif v[0] != ops[i+1][0]:

					pre_string1 = string1

					characterToDelete = string1[v[1]]

					string1=string1[:v[0]+offset]+string1[v[0]+1+offset:]

					offset -= 1

					opStrings.append([
						("Usuniecie '" + characterToDelete + "'").ljust(18),
						pre_string1, string1])
				
				else:

					pre_string1 = string1

					characterToInsert = string2[v[0]+offset]

					string1 = string1[:v[0]+offset] + \
					characterToInsert + string1[v[0]+offset:]

					offset += 1

					opStrings.append([
						("Wstawienie '" + characterToInsert + "'").ljust(18), 
						pre_string1, string1])

		return opStrings


def getEditDistanceRecursive(string1, string2):
	"""
	Funkcja zwraca stopien roznosci dwoch podanych ciagow znakowych 
	w postaci liczby calkowitej - wersja rekurencyjna
	"""

	if not isinstance(string1, str):
		string1 = str(string1)

	if not isinstance(string2, str):
		string2 = str(string2)

	if not string1: return len(string2)
	if not string2: return len(string1)

	return min(getEditDistanceRecursive(
		string1[1:], string2[1:]) + (string1[0] != string2[0]), 
		getEditDistanceRecursive(string1[1:], string2) + 1, 
		getEditDistanceRecursive(string1, string2[1:])+1)

from compiler.ast import flatten

def getClosest(inputList):
	"""
	Funkcja zwraca dwa najbardziej podobne do siebie ciagi znakowe
	sposrod listy podanej w argumencie
	"""

	if not isinstance(inputList, (list, tuple)):
		raise Exception("Podaj liste/krotke jako argument")
	else:

		inputList = [str(elem) for elem in flatten(inputList)]

		if len(inputList) < 2:
			raise Exception("Podaj przynajmniej dwuelementowa liste")
		else:

			outputList = [inputList[0], inputList[1]]
			minDiff = getEditDistance(inputList[0], inputList[1])

			for elem1 in inputList:
				for elem2 in inputList:
					if elem1 != elem2:

						editDistance = getEditDistance(elem1, elem2)

						if editDistance < minDiff:
							minDiff = editDistance
							outputList[0] = elem1
							outputList[1] = elem2

			return outputList


def getClosestToGivenWord(string1, listOfWords):
	"""
	Zwraca najbardziej podobny wyraz do podanego jako pierwszy argument
	ciagu znakow sposrod listy wyraz podanej jako drugi argument
	"""

	list1 = []

	if not isinstance(listOfWords, (list, tuple)):
		raise Exception("Podaj liste/krotke jako argument")
	else:

		listOfWords = [str(elem) for elem in flatten(listOfWords)]

		minDiff = getEditDistance(string1, listOfWords[0])

		for i in listOfWords:
			if len(string1)-2 < len(i) < len(string1)+2:

				ed = getEditDistance(i, string1)

				if ed < minDiff:
					minDiff = ed
					list1 = []
					list1.append(i)
				elif ed == minDiff:
					list1.append(i)

		list2 = []

		for word in list1:
			match = re.match( r'' + string1 + '', word)

			if match:
				list2.append(word)

		if list2 != []:
			return difflib.get_close_matches(string1, list2, 1)[0]
		else:
			return difflib.get_close_matches(string1, list1, 1)[0]


class TestEditDistance(unittest.TestCase):
	def setUp(self):
		self.list = ["drzewo", "krzew", 
					 "jablko", "gruszka", 
					 "python", "perl",
					 12, 768]

		self.startTime = time.time()

	def tearDown(self):
		timeSpent = time.time() - self.startTime

		print "%s: %.5f" % ((self.id().split("."))[-1], timeSpent)

	def test_getEditDistance(self):
		self.assertEqual(getEditDistance(self.list[0], self.list[0]), 0)
		self.assertEqual(getEditDistance(self.list[-1], self.list[-1]), 0)

		self.assertEqual(getEditDistance(self.list[0], self.list[1]), 2)
		self.assertEqual(getEditDistance(self.list[2], self.list[3]), 6)

		self.assertEqual(getEditDistance(self.list[4], self.list[5]), 5)
		self.assertEqual(getEditDistance(self.list[-1], self.list[0]), 6)

	def test_getEditDistanceRecursive(self):
		self.assertEqual(getEditDistanceRecursive(self.list[0], 
												  self.list[0]), 0)

	def test_getClosest(self):
		self.assertEqual(getClosest(self.list), ["drzewo", "krzew"])

	def test_getClosestToGivenWord(self):
		self.assertEqual(getClosestToGivenWord("pezl", self.list), "perl")


def main(argv):

	str1 = ''
	str2 = ''

	try:

	  opts, args = getopt.getopt(argv,"hf:s:",["first=","second="])

	except getopt.GetoptError:

	  print 'Sposob wywolywania: test.py -f <first> -s <second>'
	  sys.exit(2)

	for opt, arg in opts:
	      if opt == '-h':
	         print 'Sposob wywolywania: test.py -f <first> -s <second>'
	         print
	         print 'Jesli wywolasz program sposobem podanym powyzej, \
	         \notrzymasz wydruk przykladowych operacji wymaganych do \
	         \nprzeksztalcenia napisu pierwszego w drugi (wymagane \
	         \nwyrazy przynajmniej dwuznakowe).'

	         sys.exit()

	      elif opt in ("-f", "--first"):
	         str1 = arg

	      elif opt in ("-s", "--second"):
	         str2 = arg

	try:

		str1 = str(str1)
		str2 = str(str2)

		listaOperacji = getEditDistance(str1, str2, True)

		print

		for i, op in enumerate(listaOperacji):
			print str(str(i+1) + '.'), op[0], '|', op[1], '->', op[2]

	except:
		print "Cos nie wyszlo. Sprawdz podane argumenty."
		print
		print 'Sposob wywolywania: test.py -f <first> -s <second>'

if __name__ == "__main__":

	if len(argv) == 1:
		unittest.main()
	else:
		main(sys.argv[1:])